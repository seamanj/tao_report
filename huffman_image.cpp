#include "huffman_image.h"
#include <iostream>



template <typename T>
int huffman<T>::traverse(node_ptr node, string code)
{
	if (node->left == NULL && node->right == NULL)
	{
		
		if(node == root)// tj : one value
		{
			node->code = '0';
			return 1;
			
		}
		else
		{
			node->code = code;
			return 0;
			
		}
			
		
	}
	else
	{
		int left_depth = 1 + traverse(node->left, code + '0');
		int right_depth = 1 + traverse(node->right, code + '1');
		return max(left_depth, right_depth);
	}
}
template <typename T>
int huffman<T>::binary_to_decimal(const string& in)
{
	int result = 0;
	for (int i = 0; i < in.size(); i++)
		result = result * 2 + in[i] - '0';
	return result;
}
template <typename T>
string huffman<T>::decimal_to_binary(int in)
{
	string temp = "";
	string result = "";
	while (in)
	{
		temp += ('0' + in % 2);
		in /= 2;
	}
	result.append(8 - temp.size(), '0');//append '0' ahead to let the result become fixed length of 8
	for (int i = temp.size() - 1; i >= 0; i--)												
	{
		result += temp[i];
	}
	return result;
}

template <typename T>
inline void huffman<T>::build_tree(string& path, T a_code)
{//build a new branch according to the inpue code and ignore the already existed branches
	node_ptr current = root;
	for (int i = 0; i < path.size(); i++)
	{
		if (path[i] == '0')
		{
			if (current->left == NULL)
				current->left = new huffman_node<T>;
			current = current->left;
		}
		else if (path[i] == '1')
		{
			if (current->right == NULL)
				current->right = new huffman_node<T>;
			current = current->right;														 
		}
	}
	current->id = a_code;					//attach id to the leaf
}

template <typename T>
huffman<T>::huffman(const std::vector<T>&& vec_) 
: vec(vec_)
{

}

template <typename T>
huffman<T>::huffman()
{

}

template <typename T>
void huffman<T>::create_pq()
{
	for(const T& key : vec)
	{
		if(node_map.count(key))
			node_map[key]->freq++;
		else
		{
			node_map[key] = new huffman_node<T>;
			node_map[key]->id = key;
			node_map[key]->freq = 1;
		}
	}


	for( const auto & key_value : node_map)
	{
		pq.push(key_value.second);
	}


}
template <typename T>
void huffman<T>::create_huffman_tree()
{
	priority_queue<node_ptr, vector<node_ptr>, compare> temp(pq);// tj : min heap, choose min elements first to construct huffman tree. Note that higher freq node is at lower level of huffman tree
	if(temp.size() ==  1) // tj : one value
	{
		root = temp.top();
		temp.pop();
	}
	else
	{
		while (temp.size() > 1)
		{//create the huffman tree with highest frequecy characher being leaf from bottom to top
			root = new huffman_node<T>;
			root->freq = 0;
			root->left = temp.top();
			root->freq += temp.top()->freq;
			temp.pop();
			root->right = temp.top();
			root->freq += temp.top()->freq;
			temp.pop();
			temp.push(root);
		}
	}

}
template <typename T>
int huffman<T>::calculate_huffman_codes()
{
	return traverse(root, "");
}

template <typename T>
int huffman<T>::coding_save(int bits_enc, ostream& out_file)
{
	// in_file.open(in_file_name, ios::in);
	// out_file.open(out_file_name, ios::out | ios::binary);


	string in = "", s = "";
	
	int num_dict = pq.size();
	for(int i = 0; i < sizeof(T); ++i)
		in += static_cast<char> ( ((num_dict >> (i * 8)) & 0xff) );	//the first byte saves the size of the priority queue
	// tj : the size is the number of character in the dict

	in += static_cast<char>(bits_enc / 8);// tj : the second byte saves the encoding length

	// tj : save the dict
	priority_queue<node_ptr, vector<node_ptr>, compare> temp(pq);
	while (!temp.empty())
	{//get all characters and their huffman codes for output
		node_ptr current = temp.top();
		for(int i = 0; i < sizeof(T); ++i)
			in += (current->id >> 8 * i ) & 0xff;
		s.assign(bits_enc - 1 - current->code.size(), '0'); //set the codes with a fixed 128-bit string form[000����1 + real code]
		// tj : here set the fixed length to the number of the dict, which means an exetreme inbalance a huffman tree, has num_dict - 1 levels
		// 128 dict, longest 127-length code
		s += '1'; //'1' indicates the start of huffman code
		// tj : prefix with 0000, start with 1, then binary huffman code
		s.append(current->code);
		std::cout << "encode " << T(current->id) << " : " << s << std::endl;

		in += (char)binary_to_decimal(s.substr(0, 8));										
		for (int i = 0; i < bits_enc / 8 - 1; i++) // tj : 256 bit = 32 * 8 bit 
		{//cut into 8-bit binary codes that can convert into saving char needed for binary file
			s = s.substr(8);
			in += (char)binary_to_decimal(s.substr(0, 8));
		}
		// tj : convert binary string to decimal number to store in binary
		temp.pop();
	}
	s.clear();

	// tj : encode the input according to the dict, convert to decimal number to transfer the binary stream.

	for(const T& key : vec)
	{
		s += node_map[key]->code;

		while (s.size() > 8)
		{//cut into 8-bit binary codes that can convert into saving char needed for binary file
			in += (char)binary_to_decimal(s.substr(0, 8));
			s = s.substr(8);
		}
	}

	int count = 8 - s.size();
	if (s.size() < 8)
	{//append number of 'count' '0' to the last few codes to create the last byte of text
		s.append(count, '0');
	}
	in += (char)binary_to_decimal(s);															//save number of 'count' at last
	in += (char)count;// tj : the last byte records the count of filled '0'

	out_file.write(in.c_str(), in.size());
	return in.size();
}
template <typename T>
void huffman<T>::recreate_huffman_tree(const std::vector<unsigned char>& in_data)
{
	// in_file.open(in_file_name, ios::in | ios::binary);
	int num_dict = 0;	
	unsigned char size;
	int pos = 0;

	for(int i = 0; i < sizeof(T); ++i)
	{
		// in_file.read(reinterpret_cast<char*>(&size), 1);// tj : dict number
		size = in_data[pos++];
		num_dict |=  (size << i * 8);
	}
		
	
	// in_file.read(reinterpret_cast<char*>(&bytes_enc), 1);

	bytes_enc = in_data[pos++];

	root = new huffman_node<T>;
	for (int i = 0; i < num_dict; i++)
	{
		T a_code;
		a_code &= 0;
		// unsigned char h_code_c[32];	//32 unsigned char to obtain the binary code
		std::vector<unsigned char> h_code_c(bytes_enc);

		for(int i = 0; i < sizeof(T); ++i)
		{
			// in_file.read(reinterpret_cast<char*>(&size), 1); 
			size = in_data[pos++];
			a_code |= (size << i * 8);
		}
			


		// in_file.read(reinterpret_cast<char*>(h_code_c.data()), bytes_enc);

		std::copy(in_data.begin() + pos , in_data.begin() + pos + bytes_enc, h_code_c.begin());
		pos += bytes_enc;

		string h_code_s = "";
		for (int i = 0; i < bytes_enc; i++)
		{//obtain the oringinal 128-bit binary string
			h_code_s += decimal_to_binary(h_code_c[i]);
		}
		int j = 0;
		while (h_code_s[j] == '0')
		{//delete the added '000����1' to get the real huffman code
			j++;
		}
		h_code_s = h_code_s.substr(j + 1);
		build_tree(h_code_s, a_code); // tj : build the leaf node according to the character's huffman coding
		std::cout << "decode " << int(a_code) << " : " << h_code_s << std::endl;
	}
}
template <typename T>
std::vector<T> huffman<T>::decoding_save(const std::vector<unsigned char>& in_data)
{
	
	// in_file.open(in_file_name, ios::in | ios::binary);
	// out_file.open(out_file_name, ios::out);
	std::vector<T> out_file;

	int num_dict = 0;	
	unsigned char size;
	int pos = 0;

	for(int i = 0; i < sizeof(T); ++i)
	{
		// in_file.read(reinterpret_cast<char*>(&size), 1);// tj : dict number
		size = in_data[pos++];
		num_dict |=  (size << i * 8);
	}
		

	// in_file.seekg(-1, ios::end);//jump to the last one byte to get the number of '0' append to the string at last
	char count0 = in_data.back();
	// in_file.read(&count0, 1);
	// in_file.seekg( sizeof(T) + 1 + (sizeof(T)+bytes_enc) * num_dict, ios::beg);//jump to the position where text starts
	int offset = sizeof(T) + 1 + (sizeof(T)+bytes_enc) * num_dict;
	// tj : sizeof(T) for saving dict size, 1 for saving bytes_enc value,  for each dict elem, we have sizeof(T) + bytes_enc
	vector<unsigned char> text(in_data.begin() + offset, in_data.end());
	// unsigned char textseg;
	// in_file.read(reinterpret_cast<char*>(&textseg), 1);
	// while (!in_file.eof())
	// {//get the text byte by byte using unsigned char
	// 	text.push_back(textseg);
	// 	in_file.read(reinterpret_cast<char*>(&textseg), 1);
	// }


	node_ptr current = root;
	string path;
	for (int i = 0; i < text.size() - 1; i++)
	{//translate the huffman code
		path = decimal_to_binary(text[i]);
		if (i == text.size() - 2)
			path = path.substr(0, 8 - count0);
		for (int j = 0; j < path.size(); j++)// tj :decode every 8 bits, 
		{
			if (path[j] == '0')
				current = current->left;
			else
				current = current->right;
			if (current->left == NULL && current->right == NULL)
			{
				// out_file.put(current->id);
				out_file.push_back(current->id);
				current = root;
			}
		}
	}
	return std::move(out_file);
	// return out_file;
	// in_file.close();
	// out_file.close();
}


template class huffman<unsigned char>;
template class huffman<short>;