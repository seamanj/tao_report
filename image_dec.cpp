#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "huffman_image.h"
#include <cmath>
#include <algorithm>



std::vector<unsigned char> read_image_file(std::string filename, int N)
{
	std::basic_ifstream<char> file(filename, std::ios::binary);

	unsigned int image_size = N * N;
	std::vector<unsigned char> vec(image_size);
	file.read(reinterpret_cast<char*>(vec.data()), image_size);


	return vec;
}

void compute_X_image(std::vector<unsigned char>& X, 
const std::vector<unsigned char>& Y, const std::vector<short>& R, int N, int B, int i, int j)
{
	
	for (int ii = 0; ii < B; ++ii)
	{
		for (int jj = 0; jj < B; ++jj)
		{

			int idx = (i*B + ii) * N + j*B + jj;
			X[idx] = Y[idx] + R[ii * B + jj];
		}
	}
}

void decode_in_block(int b)
{

    std::unique_ptr<char[]> buf( new char[ 128 ] );
    std::snprintf( buf.get(), 128, "image_512x512_%d.enc", b);
	std::string in_filename(buf.get());

    std::snprintf( buf.get(), 128, "image_512x512_%d.dec", b);
    std::string out_filename(buf.get());



    std::string filenameY = "imageY_512x512.raw";

    std::ifstream in_file(in_filename, std::ios::in);
    int N;
    int B;

    in_file.read( (char*)(&N), sizeof(N));  // tj : read image size
    in_file.read( (char*)(&B), sizeof(B));  // tj : read block size

    int num_block = N / B;

    std::vector<unsigned char> Y = read_image_file(filenameY, N);


    std::vector<unsigned char> X(N * N);
    unsigned char ch = 0;

    // tj : read metadata
    int count = 0;
    std::vector< std::vector<bool>> metadata(num_block, std::vector<bool>(num_block, false)); 
    for(int i = 0; i < num_block; ++i)
        for(int j = 0; j < num_block; ++j)
        {
            if(count % 8 == 0)
                in_file.read( (char*)(&ch), sizeof(ch));  
            metadata[i][j] = ch & 0x80;
            ch = ch << 1;
            ++count;
        }
    

    // tj : read huffman trees' length
    in_file.seekg( -num_block * num_block * sizeof(int), ios::end);
    std::vector<int> h_offset(num_block * num_block, 0); 
    for(int i = 0; i < num_block; ++i)
        for(int j = 0; j < num_block; ++j)
        {
            in_file.read( (char*)(&h_offset[i * num_block + j]), sizeof(int));  
        }

    // tj : read huffman tree


    int base = sizeof(N)+ sizeof(B) + ceil((num_block * num_block) / 8.f);
    int offset = base;
    for(int i = 0; i < num_block; ++i)
    {
        for(int j = 0; j < num_block; ++j)
        {
            int idx = i * num_block + j;
            int h_data_len = h_offset[0];

            if(idx > 0)
            {
                offset = base + h_offset[idx - 1];
                h_data_len = h_offset[idx] - h_offset[idx-1];
            }
                
            std::cout << "offset : " << offset <<  
            " length : " << h_data_len << std::endl;


            std::vector<unsigned char> h_data(h_data_len);
        
            in_file.seekg(offset, ios::beg);
            // std::copy_n(std::istream_iterator<char>(in_file), h_length[idx], h_data.begin());
            in_file.read( (char*)(&h_data[0]), h_data_len); 
            if(metadata[i][j])
            {
                huffman<short> h;
                h.recreate_huffman_tree(h_data);
                std::cout << "==================" << std::endl;
                std::vector<short> dec_R = h.decoding_save(h_data);
                compute_X_image(X, Y, dec_R, N, B, i, j);
            }
            else
            {
                huffman<unsigned char> h;
                h.recreate_huffman_tree(h_data);
                std::cout << "==================" << std::endl;
               
                std::vector<unsigned char> dec_X = h.decoding_save(h_data);

                // tj : write to X
                for (int ii = 0; ii < B; ++ii)
                {
                    for (int jj = 0; jj < B; ++jj)
                    {

                        int idx = (i*B + ii) * N + j*B + jj;
                        X[idx] = dec_X[ii * B + jj];
                    }
                }
            }
        }
    }

    ofstream out_file(out_filename, ios::out);
    out_file.write((char*)&X[0], X.size());
    out_file.close();
    std::cout << out_filename << " is saved." << std::endl;
}

int main()
{

    // std::vector<int> Bs = { 64 };
    std::vector<int> Bs = { 2, 4, 8, 16, 32, 64, 128, 256, 512 };
    for (int b : Bs)
	{
	    decode_in_block(b);
    }

}