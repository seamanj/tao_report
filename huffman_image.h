#ifndef HUFFMAN_H
#define HUFFMAN_H
#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include <map>
using namespace std;

template <typename T>
struct huffman_node
{
	T id;																				//character 
	int freq;																				//frequency of the character
	string code;																			//huffman code for the character
	huffman_node<T>* left;
	huffman_node<T>* right;
	huffman_node()
	{//constructer
		left = right = NULL;
	}
};
// typedef huffman_node* node_ptr;



template <typename T>
class huffman
{
	using node_ptr = huffman_node<T>*;
protected:
	// node_ptr node_array[256];//array for 256 value for a gray pixel 

	map<T, node_ptr> node_map;

	node_ptr child, parent, root;
	T id;

	unsigned char bytes_enc;
	std::vector<T> vec;


	class compare
	{//a object funtion to set comparing rule of priority queue
	public:
		bool operator()(const node_ptr& c1, const node_ptr& c2) const
		{
			return c1->freq > c2->freq;
		}
	};
	priority_queue<node_ptr, vector<node_ptr>, compare> pq;	//priority queue of frequency from high to low
	// void create_node_array();																
	int traverse(node_ptr, string);	//traverse the huffman tree and get huffman code for a character
	int binary_to_decimal(const string&);//convert a 8-bit 0/1 string of binary code to a decimal integer 
	string decimal_to_binary(int);//convert a decimal integer to a 8-bit 0/1 string of binary code
	inline void build_tree(string&, T);//build the huffman tree according to information from file 

public:

	huffman(const std::vector<T>&& );
	huffman();
	void create_pq();
	void create_huffman_tree();
	int calculate_huffman_codes();
	int coding_save(int bits_enc, ostream& out_file);
	std::vector<T> decoding_save(const std::vector<unsigned char>&);
	void recreate_huffman_tree(const std::vector<unsigned char>&);
};

#endif
