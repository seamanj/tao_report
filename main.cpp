/*
 * © V-Nova International Limited 2021, all rights reserved. 
 * This code is provided by V-Nova to assess engineering candidates.
 * It should not be shared externally without prior permission from V-Nova.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <map>
#include <cmath>
#include <cassert>  

// #include <opencv2/core.hpp>
// #include <opencv2/imgcodecs.hpp>
#include <iterator>

#include "huffman_image.h"

class Results
{
	double average_image_entropy;
	double percentage_intra_blocks;
	std::vector< std::vector<bool>> metadata;

public:
	Results(double entropy, double percentage, std::vector< std::vector<bool>>&& metadata_)
		: average_image_entropy(entropy)
		, percentage_intra_blocks(percentage)
		, metadata(metadata_)
	{
	}

	void write_results(std::string filename, int B, bool write_meta = false)
	{
		std::ofstream fd;
		fd.open(filename, std::ios_base::app);
		fd << B << "," << average_image_entropy << "," << percentage_intra_blocks << std::endl;
		if(write_meta)
		{
			for(const auto& row : metadata)
			{
				copy(row.begin(), row.end(), std::ostream_iterator<int>(fd, " "));
				fd << std::endl;
			}
		}
		fd.close();
	}
};


std::vector<unsigned char> read_image_file(std::string filename, int N)
{
	std::basic_ifstream<char> file(filename, std::ios::binary);

	unsigned int image_size = N * N;
	std::vector<unsigned char> vec(image_size);
	file.read(reinterpret_cast<char*>(vec.data()), image_size);



	return vec;
}

std::vector<short> compute_residual_image(const std::vector<unsigned char>& X, const std::vector<unsigned char>& Y, int N)
{
	/*
	 * TODO: Complete this function
	 */
	std::vector<short> R(N * N);
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			//R[i, j] = .....
			int idx = i * N + j;
			R[idx] = X[idx] - Y[idx];
		}
	}
	return R;
}

template <typename T>
double calculate_entropy_of_image_block(const std::vector<T>& img, int tl_i, int tl_j, int B)
{
	/*
	 * TODO: Complete this function
	 */
	double entropy = 0.0;
	int img_size = img.size();
	int N = sqrt(img_size);
	assert( N * N == img_size); // tj : the size of the image should be square

	
	std::map<T, int> elem_count_map;

	for(int i = tl_i; i < tl_i + B; ++i)
		for(int j = tl_j; j < tl_j +B; ++j)
		{
			++elem_count_map[img[i*N + j]];
		}
	
	int B_B = B*B;

	for(auto const& elem_count : elem_count_map)
	{
		double p = static_cast<double>(elem_count.second) / B_B;
		entropy += -p * log(p);
	}


	return entropy;
}


Results adaptive_coding_algorithm(const std::vector<unsigned char>& X, const std::vector<short> &R, int B, int N)
{
	/* 
	 * TODO: Complete this function
	 */
	double entropy = 0.0;
	double percentage = 0.0;

	int num_intra = 0;
	int num_block = N / B;
	std::vector< std::vector<bool>> metadata(num_block, std::vector<bool>(num_block, false)); 
	for (int i = 0; i < N; i += B)
	{
		for (int j = 0; j < N; j += B)
		{
			double hX = calculate_entropy_of_image_block(X, i, j, B);
			double hR = calculate_entropy_of_image_block(R, i, j, B);
			if(hX <= hR)
			{
				entropy += hX;
				++num_intra;
				metadata[i / B][j / B] = false;
			}
			else
			{
				entropy += hR;
				metadata[i / B][j / B] = true;
			}
				
		}
	}
	

	entropy  = entropy / (num_block * num_block);
	percentage = static_cast<double>(num_intra) / (num_block * num_block);


	// tj : encoding
	std::unique_ptr<char[]> buf( new char[ 128 ] );
    std::snprintf( buf.get(), 128, "image_%dx%d_%d.enc", N, N, B);
	// std::string filename =  std::string( buf.get(), buf.get() + 128 - 1 );
	std::ofstream file (buf.get(), std::ios::binary);	
	file.write ((char *)&N, sizeof(N)); // tj : write image size
	file.write ((char *)&B, sizeof(B)); // tj : write block size

	// tj : write the metadata
	int count = 0;
	unsigned char ch = 0;
	for(int i = 0; i < num_block; ++i)
	{
		for(int j = 0; j < num_block; ++j)
		{

			ch = (ch << 1) + metadata[i][j];
			++count;
			if(count % 8 == 0)
			{
				file.write((char *)&ch, 1);
				ch = 0;
			}
		}
	}
	if( count % 8 != 0) // tj : fill with 0
	{
		ch = ch << (8 - count % 8);
		file.write((char *)&ch, 1);
	}

	// // tj : placehold the huffman data length
	// int h_size = 0;
	// for(int i = 0; i < num_block; ++i)
	// {
	// 	for(int j = 0; j < num_block; ++j)
	// 	{
	// 		file.write ((char *)&h_size, sizeof(h_size)); 
	// 	}
	// }

	// tj : write huffman tree in block
	int offset = 0;
	// std::vector< std::vector<int>> h_length(num_block, std::vector<int>(num_block, 0)); 
	std::vector<int> h_offset(num_block * num_block, 0);
	for(int i = 0; i < num_block; ++i)
		for(int j = 0; j < num_block; ++j)
		{
			if(metadata[i][j]) // tj : process R
			{

				std::vector<short> vec;

				for(int ii = i * B; ii < i * B + B; ++ii)
					for(int jj = j*B; jj < j*B +B; ++jj)
					{
						vec.emplace_back(R[ii * N + jj]);
					}

				huffman<short> h(std::move(vec));
				h.create_pq();// tj : count the freq for each character in the dict
				h.create_huffman_tree(); // tj : create the huffman tree based on the freq
				int max_depth = h.calculate_huffman_codes(); // tj : encode for each character (leaf node in the huffman tree)
				int bits_enc = std::ceil ( static_cast<float>(max_depth + 1) / 8 ) * 8; // tj : + 1 for the sentinel
				offset += h.coding_save(bits_enc, file);
				h_offset[i * num_block + j] = offset;
				std::cout << "=========R=========" << std::endl;
			}
			else // tj : process X
			{
				std::vector<unsigned char> vec;

				for(int ii = i * B; ii < i * B + B; ++ii)
					for(int jj = j*B; jj < j*B +B; ++jj)
					{
						vec.emplace_back(X[ii * N + jj]);
					}

				huffman<unsigned char> h(std::move(vec));
				h.create_pq();// tj : count the freq for each character in the dict
				h.create_huffman_tree(); // tj : create the huffman tree based on the freq
				int max_depth = h.calculate_huffman_codes(); // tj : encode for each character (leaf node in the huffman tree)
				int bits_enc = std::ceil ( static_cast<float>(max_depth + 1) / 8 ) * 8; // tj : + 1 for the sentinel
				offset += h.coding_save(bits_enc, file);
				h_offset[i * num_block + j] = offset;
				std::cout << "=========X=========" << std::endl;
			}
		}

	// tj : write the real huffman data length

	// tj : locate to huffman length 
	// file.seekg(sizeof(N)+ sizeof(B) + ceil ((num_block * num_block) / 8.f) * 8, ios::beg);
	for(int i = 0; i < num_block; ++i)
	{
		for(int j = 0; j < num_block; ++j)
		{
			file.write ((char *)&h_offset[i * num_block + j], sizeof(int)); 
		}
	}

	file.close();
	std::cout << buf.get() << " is saved." << std::endl;

	Results res(entropy, percentage, std::move(metadata));
	return res;
}

int main(int argc, char* argv[])
{
	int N = 512;
	int B = atoi(argv[1]);

	std::cout << "Image dimensions (NxN): " << N << "x" << N << std::endl;
	std::cout << "Block size (NxN): " << B << "x" << B << std::endl;

	if (N % B != 0)
	{
		std::cout << "Not valid value of block size B" << std::endl;
		return -1;
	}
	
	std::string filenameX = "imageX_512x512.raw";
	std::string filenameY = "imageY_512x512.raw";
	std::string output_filename = "results.txt";

	// Read images X and Y
	std::vector<unsigned char> X = read_image_file(filenameX, N);
	std::vector<unsigned char> Y = read_image_file(filenameY, N);

	// copy(X.begin(), X.end(), std::ostream_iterator<int>(std::cout, " "));

	// cv::Mat matX(N, N, CV_8UC1, X.data());
	// // std::cout << matX << std::endl;
	// cv::imwrite("X.png", matX);

	// cv::Mat matY(N, N, CV_8UC1, Y.data());
	// cv::imwrite("Y.png", matY);

	std::vector<short> R = compute_residual_image(X, Y, N);

	std::vector<int> Bs = { 2, 4, 8, 16, 32, 64, 128, 256, 512 };
	// std::vector<int> Bs = { 64 };
	for (int k = 0; k < Bs.size(); k++)
	{
		B = Bs[k];
		Results res = adaptive_coding_algorithm(X, R, B, N);
		res.write_results(output_filename, B, false);
	}
	
	return 0;
}

